class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.string :need
      t.string :resolution
      t.string :issue
      t.string :additionallinks

      t.timestamps null: false
    end
  end
end
